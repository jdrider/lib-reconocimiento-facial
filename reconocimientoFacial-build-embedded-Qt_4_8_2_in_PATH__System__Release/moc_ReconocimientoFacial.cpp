/****************************************************************************
** Meta object code from reading C++ file 'ReconocimientoFacial.h'
**
** Created: Thu Oct 2 21:17:02 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../reconocimientoFacial/ReconocimientoFacial.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ReconocimientoFacial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ReconocimientoFacial[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   54,   67,   67, 0x05,
      68,   88,   67,   67, 0x05,

 // slots: signature, parameters, type, tag, flags
      95,   67,   67,   67, 0x0a,
     116,   67,   67,   67, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ReconocimientoFacial[] = {
    "ReconocimientoFacial\0"
    "lanzarImagenReco(QImage,QString)\0"
    "image,nombre\0\0detenerCamara(bool)\0"
    "estado\0capturarImagenReco()\0"
    "detenerReconocimiento()\0"
};

void ReconocimientoFacial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ReconocimientoFacial *_t = static_cast<ReconocimientoFacial *>(_o);
        switch (_id) {
        case 0: _t->lanzarImagenReco((*reinterpret_cast< QImage(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->detenerCamara((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->capturarImagenReco(); break;
        case 3: _t->detenerReconocimiento(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ReconocimientoFacial::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ReconocimientoFacial::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ReconocimientoFacial,
      qt_meta_data_ReconocimientoFacial, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ReconocimientoFacial::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ReconocimientoFacial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ReconocimientoFacial::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ReconocimientoFacial))
        return static_cast<void*>(const_cast< ReconocimientoFacial*>(this));
    return QObject::qt_metacast(_clname);
}

int ReconocimientoFacial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ReconocimientoFacial::lanzarImagenReco(QImage _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ReconocimientoFacial::detenerCamara(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
