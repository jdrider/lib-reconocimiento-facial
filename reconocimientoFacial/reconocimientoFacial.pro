#-------------------------------------------------
#
# Project created by QtCreator 2014-09-08T03:33:06
#
#-------------------------------------------------

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = reconocimientoFacial
TEMPLATE = lib

DEFINES += RECONOCIMIENTOFACIAL_LIBRARY

SOURCES += ReconocimientoFacial.cpp \
    CamaraRPI.cpp \
    subspace.cpp \
    helper.cpp \
    EigenvalueDecomposition.cpp \
    Eigenfaces.cpp \
    TratamientoImagenes.cpp

HEADERS += ReconocimientoFacial.h\
    reconocimientoFacial_global.h\
    CamaraRPI.h \
    subspace.h \
    helper.h \
    EigenvalueDecomposition.h \
    Eigenfaces.h \
    TratamientoImagenes.h \
    FaceRecognizerP.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE4882857
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = reconocimientoFacial.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/local/lib/ -lraspicam_cv

INCLUDEPATH += $$PWD/../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../usr/local/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/local/lib/ -lraspicam

INCLUDEPATH += $$PWD/../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../usr/local/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/lib/ -lopencv_core

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/lib/ -lopencv_highgui


unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/lib/ -lopencv_objdetect

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../usr/lib/ -lopencv_imgproc

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include
