#include "CamaraRPI.h"
#include <iostream>
#include <raspicam/raspicam_cv.h>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <QImage>

#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.h>

using namespace std;

CamaraRPI::CamaraRPI(int argc, char **argv, QObject *parent):
    QThread(parent),
    argc(argc),
    argv(argv)
{
    estado = true;
    timeSleep = 100;
}
void CamaraRPI::processCommandLine(int argc, char **argv, raspicam::RaspiCam_Cv &Camera)
{
    Camera.set ( CV_CAP_PROP_FRAME_WIDTH,  getParamVal ( "-w",argc,argv,320 ) );
    Camera.set ( CV_CAP_PROP_FRAME_HEIGHT, getParamVal ( "-h",argc,argv,240 ) );
    Camera.set ( CV_CAP_PROP_BRIGHTNESS,getParamVal ( "-br",argc,argv,50 ) );
    Camera.set ( CV_CAP_PROP_CONTRAST ,getParamVal ( "-co",argc,argv,50 ) );
    Camera.set ( CV_CAP_PROP_SATURATION, getParamVal ( "-sa",argc,argv,50 ) );
    Camera.set ( CV_CAP_PROP_GAIN, getParamVal ( "-g",argc,argv ,50 ) );
    if ( findParam ( "-gr",argc,argv ) !=-1 )
        Camera.set ( CV_CAP_PROP_FORMAT, CV_8UC1 );
    //if ( findParam ( "-test_speed",argc,argv ) !=-1 )
      //  doTestSpeedOnly=true;
    if ( findParam ( "-ss",argc,argv ) !=-1 )
        Camera.set ( CV_CAP_PROP_EXPOSURE, getParamVal ( "-ss",argc,argv )  );
}

float CamaraRPI::getParamVal(QString param, int argc, char **argv, float defvalue)
{
    int idx=-1;
        for ( int i=0; i<argc && idx==-1; i++ )
            if ( QString ( argv[i] ) ==param ) idx=i;
        if ( idx==-1 ) return defvalue;
        else return atof ( argv[  idx+1] );
}

int CamaraRPI::findParam(QString param, int argc, char **argv)
{
    int idx=-1;
        for ( int i=0; i<argc && idx==-1; i++ )
            if ( QString ( argv[i] ) ==param ) idx=i;
        return idx;
}


void CamaraRPI::run()
{

    //int i =0;
    raspicam::RaspiCam_Cv Camera;
    processCommandLine ( argc,argv,Camera );
    if (Camera.open()) {
        cout<<"Connected to camera ="<<Camera.getId() <<endl;
        //cv::Mat image;
        while(estado)
        {
            Camera.grab();
            Camera.retrieve ( image );
            //cout<<"\r capturing ..."<<++i<<std::flush;
            //cv::Mat clone;
            //cvtColor(image,clone, CV_BGR2RGB);
            //QImage img ((uchar*)clone.data, clone.cols, clone.rows, QImage::Format_RGB888);
            emit lanzarImagen();
            msleep(timeSleep);
        }
    }
    Camera.release();
}

cv::Mat CamaraRPI::getImage()
{
    return this->image;
}

void CamaraRPI::setTimeSleep(int time)
{
    timeSleep = time;
}


void CamaraRPI::cambiarEstadoCamara(bool estadoCamara)
{
    this->estado = estadoCamara;
}
