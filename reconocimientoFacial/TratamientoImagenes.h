#ifndef TRATAMIENTOIMAGENES_H
#define TRATAMIENTOIMAGENES_H

#include <QObject>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "reconocimientoFacial_global.h"

#include <iostream>
#include <stdio.h>
#include <dirent.h>

using namespace cv;
using namespace std;
class RECONOCIMIENTOFACIALSHARED_EXPORT TratamientoImagenes : public QObject
{
    Q_OBJECT
public:
    explicit TratamientoImagenes(QObject *parent = 0);
    float Distance(CvPoint p1, CvPoint p2);
    cv::Mat rotate(Mat& image, double angle, CvPoint centre);
    int CropFace(Mat &MyImage, CvPoint eye_left, CvPoint eye_right, CvPoint offset_pct, CvPoint dest_sz);
    void resizePicture(Mat& src, int coeff);
    int detectAndDisplay( Mat frame );
    void generarImagenesRecoFacial(string directorioOrigen,string directorioDestino, string nombre);
private:
    CascadeClassifier face_cascade;
    CascadeClassifier eyes_cascade;
    CascadeClassifier glasses_cascade;
    CvPoint Myeye_left;
    CvPoint Myeye_right;
    int bEqHisto;
};

#endif // TRATAMIENTOIMAGENES_H
