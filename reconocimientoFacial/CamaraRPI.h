#ifndef CAMARARPI_H
#define CAMARARPI_H

#include <QThread>
#include <QString>
#include <raspicam/raspicam_cv.h>
#include <QImage>
#include "reconocimientoFacial_global.h"

class RECONOCIMIENTOFACIALSHARED_EXPORT CamaraRPI: public QThread
{
    Q_OBJECT
public:
    explicit CamaraRPI(int argc, char **argv, QObject *parent);
    void processCommandLine ( int argc,char **argv,raspicam::RaspiCam_Cv &Camera );
    float getParamVal ( QString param,int argc,char **argv,float defvalue=-1 );
    int findParam ( QString param,int argc,char **argv );
    void setEstado(bool estado);
    cv::Mat getImage();
    void setTimeSleep(int time);

protected:
    virtual void run();
private:
    int argc;
    char **argv;
    bool estado;
    cv::Mat image;
    int timeSleep;
signals:
    void lanzarImagen();
public slots:
    void cambiarEstadoCamara(bool estadoCamara);
};

#endif // CAMARARPI_H
