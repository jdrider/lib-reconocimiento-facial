#ifndef FACERECOGNIZERP_H
#define FACERECOGNIZERP_H

#include <opencv2/core/core.hpp>
#include "reconocimientoFacial_global.h"

using namespace std;

namespace cv {

    class RECONOCIMIENTOFACIALSHARED_EXPORT FaceRecognizerP{
    public:

        //! virtual destructor
        virtual ~FaceRecognizerP() {}

        // Trains a FaceRecognizer.
        virtual void train(InputArray src, InputArray labels) = 0;

        // Gets a prediction from a FaceRecognizer.
        virtual int predict(InputArray src) const = 0;

        // Gets a prediction from a FaceRecognizer.
        virtual void predict(InputArray src, int &label, double &confidence) const = 0;

        // Serializes this object to a given filename.
        virtual void save(const string& filename) const;

        // Deserializes this object from a given filename.
        virtual void load(const string& filename);

        // Serializes this object to a given cv::FileStorage.
        virtual void save(FileStorage& fs) const = 0;

        // Deserializes this object from a given cv::FileStorage.
        virtual void load(const FileStorage& fs) = 0;
    };
}
#endif // FACERECOGNIZERP_H
