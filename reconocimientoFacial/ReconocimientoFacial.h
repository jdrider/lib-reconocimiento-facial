#ifndef RECONOCIMIENTOFACIAL_H
#define RECONOCIMIENTOFACIAL_H

#include "reconocimientoFacial_global.h"

#include <QString>
#include <QVector>

#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <fstream>
#include <sstream>



#include <QImage>
#include <QObject>
#include "CamaraRPI.h"
#include "Eigenfaces.h"

using namespace std;
using namespace cv;

class RECONOCIMIENTOFACIALSHARED_EXPORT ReconocimientoFacial: public QObject
{
    Q_OBJECT
public:
    explicit ReconocimientoFacial(string archivo,int argc, char **argv,QObject *parent=0);
    void cargar_csv(std::string &filename,char separator = ';');
    void iniciarReconocimiento();
    void inicializar();
private:
    CamaraRPI *camara;
    vector<Mat> images;
    vector<int> labels;
    vector<string> nombres;
    Eigenfaces model;
    CascadeClassifier face_cascade;
    int width, height;
    int PREDICTION_SEUIL;
public slots:
    void capturarImagenReco();
    void detenerReconocimiento();
signals:
    void lanzarImagenReco(QImage image, QString nombre);
    void detenerCamara(bool estado);
};

#endif // RECONOCIMIENTOFACIAL_H

