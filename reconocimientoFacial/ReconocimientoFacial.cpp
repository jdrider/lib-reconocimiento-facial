#include "ReconocimientoFacial.h"


#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <iostream>
#include <fstream>
#include <sstream>



using namespace std;
using namespace cv;

ReconocimientoFacial::ReconocimientoFacial(string archivo, int argc, char **argv, QObject *parent):QObject(parent)
{
    camara = new CamaraRPI(argc, argv, this);

    connect(camara, SIGNAL(lanzarImagen()), this, SLOT(capturarImagenReco()));
    connect(this, SIGNAL(detenerCamara(bool)),camara,SLOT(cambiarEstadoCamara(bool)));
    PREDICTION_SEUIL= 4000;
    cargar_csv(archivo, ';');
    inicializar();
}
/**
 * @brief ReconocimientoFacial::cargar_csv
 * @param filename
 * @param images
 * @param labels
 * @param separator
 */
void ReconocimientoFacial::cargar_csv(string& filename, char separator)
{
    ifstream file(filename.c_str(), ifstream::in);

    if(!file){
        QString error_message = "Archivo invalido, por favor revise que el archivo existe";
        CV_Error(CV_StsBadArg, error_message.toStdString());
    }

    string line, path, classlabel, name;
    while(getline(file, line)){
        stringstream lines(line);
        getline(lines,path,separator);
        getline(lines, classlabel,separator);
        getline(lines, name);
        if(!path.empty()&& !classlabel.empty()){
            cout << "is pushinggg back";
            cout << " at path" << path;
            cout << " defPathaaa: " << path.c_str();
            IplImage* img = cvLoadImage("/home/pi/Registro/Reconocimiento/juan2/Reco/juan247.jpg");
            Mat mat = Mat(img);
            imshow("primera", mat);
            images.push_back(mat);
            labels.push_back(atoi(classlabel.c_str()));
            if(!nombres.empty()){
                if(find(nombres.begin(), nombres.end(), name) == nombres.end()){
                    nombres.push_back(name);
                }
            }
            else
            {
                nombres.push_back(name);
            }
        }
    }
    cout << "flag cargo CSV";
    //cout<<"\n nombres ..."<<nombres.size()<<std::flush;
}

void ReconocimientoFacial::iniciarReconocimiento()
{

    emit detenerCamara(true);
    camara->start();
}

void ReconocimientoFacial::inicializar()
{
    height=images[0].rows;
    width=images[0].cols;
    cout << " pass first train";
    model.train(images,labels);
    cout << "hizo train";

        //string eyes_cascade_name ="/usr/share/opencv/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
    //string glasses_cascade ="/usr/share/opencv/haarcascades/haarcascade_eye.xml";
    //string frontal_cascade_name ="/usr/share/opencv/haarcascades/haarcascade_frontalface_alt.xml";
    string frontal_cascade_name ="/usr/share/opencv/lbpcascades/lbpcascade_frontalface.xml";

    face_cascade.load(frontal_cascade_name);
    cout << "cargo face cascade";
}

void ReconocimientoFacial::capturarImagenReco()
{
    cout << "Arranco capturarimagen";
    bool deteccionFace =false;
    Mat redimensionada;
    Mat gris;
    cv::Mat clone;
    //conversion de QImage to cv::Mat
    //Mat original = cv::Mat(image.height(), image.width(), CV_8UC3, (uchar*)image.bits(), image.bytesPerLine()).clone();
    //conversion a escala a grises
    Mat original = camara->getImage();
    cv::cvtColor(original,gris,CV_BGR2GRAY);

    vector< Rect_<int> > faces;
    face_cascade.detectMultiScale(gris, faces,1.1,3, CV_HAAR_SCALE_IMAGE, Size(80,80));

    for(int i =0; i< faces.size();i++){
        Rect face_i = faces[i];
        Mat face = gris(face_i);

        cv::resize(face,redimensionada,Size(width,height),1.0,1.0,CV_INTER_NN);

        int prediccion =-1;
        double prediccion_confidencial = 0.0;
        model.predict(redimensionada,prediccion,prediccion_confidencial);
        rectangle(original, face_i, CV_RGB(0,255,0),1);
        //cout<<"\n prediccion ..."<<prediccion<<std::flush;
        //cout<<"\n nombre ..."<<nombres[prediccion]<<std::flush;
        deteccionFace=true;
        int pos_x= max(face_i.tl().x-10,0);
        int pos_y= max(face_i.tl().y-10,0);

        if(prediccion_confidencial > PREDICTION_SEUIL){
            putText(original, nombres[prediccion], Point(pos_x,pos_y),FONT_HERSHEY_PLAIN,1.0,CV_RGB(255,255,255),1.0);
            cvtColor(original,clone, CV_BGR2RGB);
            QImage image ((uchar*)clone.data, clone.cols, clone.rows, QImage::Format_RGB888);
            emit lanzarImagenReco(image, QString::fromStdString(nombres[prediccion]));
        }else{
            QString temp ="Desconocido";

            putText(original, "Desconocido", Point(pos_x,pos_y),FONT_HERSHEY_PLAIN,1.0,CV_RGB(255,255,255),1.0);
            cvtColor(original,clone, CV_BGR2RGB);
            QImage image ((uchar*)clone.data, clone.cols, clone.rows, QImage::Format_RGB888);
            emit lanzarImagenReco(image, temp);
        }
    }
    //imagen con recuadro en el rostro
    if(!deteccionFace){
        //si no hay deteccion de rostro
        //cout<<"\n FALLIDA"<<std::flush;
        cvtColor(original,clone, CV_BGR2RGB);
        QImage image ((uchar*)clone.data, clone.cols, clone.rows, QImage::Format_RGB888);
        QString temp ="";
        emit lanzarImagenReco(image, temp);
    }
}

void ReconocimientoFacial::detenerReconocimiento()
{
    emit detenerCamara(false);
}


