#include "Eigenfaces.h"
#include "helper.h"
#include "EigenvalueDecomposition.h"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

void cv::FaceRecognizerP::save(const string& filename) const {
    FileStorage fs(filename, FileStorage::WRITE);
    if (!fs.isOpened())
        CV_Error(CV_StsError, "File can't be opened for writing!");
    this->save(fs);
    fs.release();
}

void cv::FaceRecognizerP::load(const string& filename) {
    FileStorage fs(filename, FileStorage::READ);
    if (!fs.isOpened())
        CV_Error(CV_StsError, "File can't be opened for writing!");
    this->load(fs);
    fs.release();
}

void cv::Eigenfaces::train(cv::InputArray src, cv::InputArray _lbls)
{
    if(src.total() == 0) {
        string error_message = format("Empty training data was given. You'll need more than one sample to learn a model.");
        CV_Error(CV_StsUnsupportedFormat, error_message);
    } else if(_lbls.getMat().type() != CV_32SC1) {
        string error_message = format("Labels must be given as integer (CV_32SC1). Expected %d, but was %d.", CV_32SC1, _lbls.type());
        CV_Error(CV_StsUnsupportedFormat, error_message);
    }
    // make sure data has correct size
    if(src.total() > 1) {
        for(int i = 1; i < src.total(); i++) {
            if(src.getMat(i-1).total() != src.getMat(i).total()) {
                string error_message = format("In the Eigenfaces method all input samples (training images) must be of equal size! Expected %d pixels, but was %d pixels.", src.getMat(i-1).total(), src.getMat(i).total());
                CV_Error(CV_StsUnsupportedFormat, error_message);
            }
        }
    }
    cout << "inicializando... ";
    // get labels
    vector<int> labels = _lbls.getMat();
    // observations in row
    cout << "va a crear la matriz... ";
    Mat data = asRowMatrix(src, CV_64FC1);
    cout << "ya creo la matriz...";
    // number of samples
    int n = data.rows;
    // dimensionality of data
    int d = data.cols;
    cout << "pidio filas y columnas... ";
    // assert there are as much samples as labels
    if(n != labels.size()) {
        string error_message = format("The number of samples (src) must equal the number of labels (labels). Was len(samples)=%d, len(labels)=%d.", n, labels.size());
        CV_Error(CV_StsBadArg, error_message);
    }
    cout << "despues de mirar el label size... ";
    // clip number of components to be valid
    if((_num_components <= 0) || (_num_components > n))
        _num_components = n;
    // perform the PCA
    cout << "perform pca ...";
    PCA pca(data, Mat(), CV_PCA_DATA_AS_ROW , _num_components);
    // copy the PCA results
    _mean = pca.mean.reshape(1,1); // store the mean vector
    _eigenvalues = pca.eigenvalues.clone(); // eigenvalues by row
    _eigenvectors = transpose(pca.eigenvectors); // eigenvectors by column
    _labels = labels; // store labels for prediction
    // save projections
    cout << "antes del for... ";
    for(int sampleIdx = 0; sampleIdx < data.rows; sampleIdx++) {
        Mat p = subspace::project(_eigenvectors, _mean, data.row(sampleIdx).clone());
        this->_projections.push_back(p);
    }
}

int cv::Eigenfaces::predict(const cv::InputArray _src) const
{

    int label=-1;
    double dummy;
    predict(_src, label, dummy);
    return label;
}

void cv::Eigenfaces::predict(cv::InputArray _src, int &minClass, double &minDist) const
{
    Mat src = _src.getMat();
    if(_projections.empty()) {
        // throw error if no data (or simply return -1?)
        string error_message = "This cv::Eigenfaces model is not computed yet. Did you call cv::Eigenfaces::train?";
        CV_Error(CV_StsError, error_message);
    } else if(_eigenvectors.rows != src.total()) {
        // check data alignment just for clearer exception messages
        string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
        CV_Error(CV_StsError, error_message);
    }
    // project into PCA subspace
    Mat q = subspace::project(_eigenvectors, _mean, src.reshape(1,1));
    // find 1-nearest neighbor
    minDist = DBL_MAX;
    minClass = -1;
    for(int sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
        double dist = norm(_projections[sampleIdx], q, NORM_L2);
        if((dist < minDist) && (dist < _threshold)) {
            minDist = dist;
            minClass = _labels[sampleIdx];
        }
    }
}

void cv::Eigenfaces::load(const cv::FileStorage& fs) {
    //read matrices
    fs["num_components"] >> _num_components;
    fs["mean"] >> _mean;
    fs["eigenvalues"] >> _eigenvalues;
    fs["eigenvectors"] >> _eigenvectors;
    // read sequences
    readFileNodeList(fs["projections"], _projections);
    readFileNodeList(fs["labels"], _labels);
}

void cv::Eigenfaces::save(cv::FileStorage &fs) const
{
    // write matrices
    fs << "num_components" << _num_components;
    fs << "mean" << _mean;
    fs << "eigenvalues" << _eigenvalues;
    fs << "eigenvectors" << _eigenvectors;
    // write sequences
    writeFileNodeList(fs, "projections", _projections);
    writeFileNodeList(fs, "labels", _labels);
}

