#ifndef EIGENFACES_H
#define EIGENFACES_H

#include "opencv2/core/core.hpp"
#include "FaceRecognizerP.h"
#include "helper.h"
#include "subspace.h"
#include "reconocimientoFacial_global.h"

using namespace std;

namespace cv {

// Turk, M., and Pentland, A. "Eigenfaces for recognition.". Journal of
// Cognitive Neuroscience 3 (1991), 71–86.
    class RECONOCIMIENTOFACIALSHARED_EXPORT Eigenfaces : public FaceRecognizerP {

    private:
        int _num_components;
        double _threshold;

        vector<Mat> _projections;
        vector<int> _labels;
        Mat _eigenvectors;
        Mat _eigenvalues;
        Mat _mean;


    public:
        using FaceRecognizerP::save;
        using FaceRecognizerP::load;

        // Initializes an empty Eigenfaces model.
        Eigenfaces(int num_components = 0, double threshold = DBL_MAX) :
            _num_components(num_components),
            _threshold(threshold) { }

        // Initializes and computes an Eigenfaces model with images in src and
        // corresponding labels in labels. num_components will be kept for
        // classification.
        Eigenfaces(InputArray src, InputArray labels,
                int num_components = 0, double threshold = DBL_MAX) :
            _num_components(num_components),
            _threshold(threshold) {
            train(src, labels);
        }

        // Computes an Eigenfaces model with images in src and corresponding labels
        // in labels.
        void train(InputArray src, InputArray labels);

        // Predicts the label of a query image in src.
        int predict(const InputArray _src) const;

        // Returns the predicted label and confidence for the prediction.
        void predict(InputArray src, int &minClass, double &minDist) const;

        // See cv::FaceRecognizer::load.
        void load(const FileStorage& fs);

        // See cv::FaceRecognizer::save.
        void save(FileStorage& fs) const;

        // Returns the eigenvectors of this PCA.
        Mat eigenvectors() const { return _eigenvectors; }

        // Returns the eigenvalues of this PCA.
        Mat eigenvalues() const { return _eigenvalues; }

        // Returns the sample mean of this PCA.
        Mat mean() const { return _mean; }

        // Returns the number of components used in this PCA.
        int num_components() const { return _num_components; }

        // Returns the threshold used in this cv::Eigenfaces.
        double getThreshold() const { return _threshold; }

        // Sets the threshold used in this cv::Eigenfaces.
        void setThreshold(double threshold) { _threshold = threshold; }

    };
}
#endif // EIGENFACES_H
